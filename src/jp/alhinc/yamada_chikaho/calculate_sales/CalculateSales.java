package jp.alhinc.yamada_chikaho.calculate_sales;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.FilenameFilter;
import java.io.IOException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;;

//売上ファイルネームフィルター
class FileFilter implements FilenameFilter {
	public boolean accept(File dir, String name) {
		if (name.matches("^\\d{8}.rcd$")) {
			return true;
		}
		return false;
	}
};

public class CalculateSales {

	public static void main(String[] args) {

		//System.out.println("ここにあるファイルを開きます =>" + args[0]);
		BufferedReader br = null;
		Map<String, String> branchMap = new HashMap<>();
		Map<String, Long> map = new HashMap<>();
		Map<String, String> readMap = new HashMap<>();
		String[] resultArray = {};
		String code = "";

		//【支店定義ファイルの読み込みと保持】
		try {
			File file = new File(args[0], "branch.lst");
			//《エラー処理》支店定義ファイルの有無
			if (!file.exists()) {
				System.out.println("支店定義ファイルが存在しません");
				return;
			}

			FileReader fr = new FileReader(file);

			br = new BufferedReader(fr);
			String line;
			while ((line = br.readLine()) != null) {
				resultArray = line.split(",");
				//《エラー処理》フォーマットチェック
				if (!resultArray[0].matches("^\\d{3}$") || resultArray.length != 2) {
					System.out.println("支店定義ファイルのフォーマットが不正です");
					return;
					//0円mapの作成(売上ファイル合算用)
				} else {
					branchMap.put(resultArray[0], resultArray[1]);
					map.put(resultArray[0], 0L);
				}
			}
			fr.close();

		} catch (IOException e) {
			System.out.println("予期せぬエラーが発生しました");
			return;
		} finally {
			if (br != null) {
				try {
					br.close();
				} catch (IOException e) {
					System.out.println("予期せぬエラーが発生しました");
					return;
				}
			}
		}


		//【集計】
		String path = args[0];
		//同ディレクトリ内のファイルを全て取得,フィルタリング
		File dir = new File(path);
		File[] fil = dir.listFiles(new FileFilter());

		Arrays.sort(fil);
		String number = fil[0].getName();
		String[] numbers = number.split("\\.");
		int length =fil.length-1 ;
		int min  = Integer.parseInt(numbers[0]);
		String number1 = fil[length].getName();
		String[] numbers1 = number1.split("\\.");
		int max = Integer.parseInt(numbers1[0]);
		if(min + length != max) {
			System.out.println("売上ファイル名が連番になっていません");
			return;
		}
		for (int i = 0; i < fil.length; i++) {

			//抽出したファイルの読み取り,mapに格納
			try {

				FileReader fr1 = new FileReader(fil[i]);

				br = new BufferedReader(fr1);
				code = br.readLine();
				String amount = br.readLine();

				//《エラー処理》3行以上NG
				if ((br.readLine()) != null) {
					System.out.println(fil[i].getName() + "のフォーマットが不正です");
					return;
				}
				//《エラー処理》支店該当なし
				if (!(branchMap.containsKey(code))) {
					System.out.println( fil[i].getName() +"の支店コードが不正です");
					return;
				}else {
					readMap.put(code, amount);
					//0円ファイルに金額を入れる
					map.put(code, map.get(code) + Long.parseLong(amount));
				}
				//《エラー処理》10桁の数字NG
				if (map.get(code) + Long.parseLong(amount) > 9999999999L) {
					System.out.println("合計金額が10桁を超えました");
					return;
				}
				fr1.close();

			} catch (IOException e) {
				System.out.println("予期せぬエラーが発生しました");
				return;
			} finally {
				if (br != null) {
					try {
						br.close();
					} catch (IOException e) {
						System.out.println("予期せぬエラーが発生しました");
						return;
					}
				}
			}
		}

		//支店別集計ファイルへ転記
		File file = new File(args[0], "branch.out");
		BufferedWriter bw;
		try {
			bw = new BufferedWriter(new FileWriter(file));
			for (String key : map.keySet()) {
				bw.write(key + "," + branchMap.get(key) + "," + map.get(key));
				bw.newLine();
			}
			bw.close();

		} catch (IOException e) {
			System.out.println("予期せぬエラーが発生しました");
			return;
		}
	}
}
